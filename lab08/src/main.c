/*
 * @mainpage
 * # Загальне завдання:
 * Сформувати функції, беручи за основу
 * лабораторні роботи №5 і №6.
 *
 *
 * @author Zheltuhina A.
 * @date 22-dec-2021
 * @version 1.0
 */

/**
 * @file main.c
 * @brief Файл, який дає можливість викликати
 * одну з двох функцій.
 *
 * @author Zheltuhina A.
 * @date 20-dec-2021
 * @version 1.0
 */
#include <stddef.h>

/**
 * @brief Знаходження числа, яке дiлиться на 7 без остачi у заданному дiапазонi
 *
 * @param min - мiнiмальний дiапазон
 * @param max - максимальний дiапазон
 * @return результат типу int
 */
int func1 (int min, int max) {
int result1;
for (int i= min; i< max+1; i++) {
    if (i%7 == 0)
       {
      if (i > result)
        result1=i;
       }
  }
return result1;
}

int main () {
func1 (10, 116);

}

/**
 * @brief Знаходження середнього арифметичного числа в  масиві
 *
 * @param len - довжина масиву
 * @return result - результат знаходження числа
 */
int func2 (size_t len) {

 /**
 * Максимальна можлива розмірність масиву
 */
#define ARR_LEN 10

srand(time(NULL));
int sum = 0;
int array[ARR_LEN];
int result2;
array[len] = rand ();
for (int i = 0; i< len; i++) {
         sum += array[i];
     }
  result = sum / len;
  return result2;
}

/**
 * @brief виклик функцій для виконання
 *
 */

int main () {
func1 (10, 116);
func2 (10);

}
